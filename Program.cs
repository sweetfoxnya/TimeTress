﻿using System;
using System.IO;
using System.Linq;
using System.Globalization;
using Newtonsoft.Json;

namespace TimeTrees
{
    class Program
    {
        struct Person
        {
            public int Id;
            public string Name;
            public DateTime Birth;
            public DateTime Death;
        }

        struct TimeLine
        {
            public DateTime Date;
            public string EventDescription;
            public string Emotions;
        }
        
        private const int IdIndex = 0;
        private const int NameIndex = 1;
        private const int BirthIndex = 2;
        private const int DeathIndex = 3;

        private const int TimeLineDateIndex = 0;
        private const int TimeLineEventDescriptionIndex = 1;
        private const int TimeLineEmotionsIndex = 2;

        public static void Main(string[] args)
        {

            string[] text = File.ReadAllLines(@"../../../timelime.csv"); // File-структура, у которой есть метод для считывания строк файла
            string[] text1 = File.ReadAllLines(@"../../../people.csv");


            DateTime maxDate = new DateTime(), minDate = new DateTime();
            object[][] textline = new object[text.Length][];
            object[][] text1people = new object[text1.Length][];


            for (int i = 0; i < text.Length; i++)
            {
                textline[i] = text[i].Split(';');

                object[] element = new object[text.Length];
                element[TimeLineDateIndex] = textline[i][TimeLineDateIndex];

                var dt = stringToDateTime((string)element[TimeLineDateIndex]); 
                if (i == 0)
                {
                   
                    maxDate = DateTime.MinValue;
                    minDate = DateTime.MaxValue;
                    
                }

                if (maxDate < dt)
                {
                    maxDate = dt;
                }

                if (minDate > dt)
                {
                    minDate = dt;
                }
            }
            Console.WriteLine(maxDate);
            Console.WriteLine(minDate);
            int[] output = ReturnDate(maxDate - minDate);

            Console.WriteLine($"Между макс и мин датами прошло: {output[0]} лет, {output[1]} месяцев и {output[2]} дней");

            for (int a = 0; a < text1.Length; a++)
            {
                text1people[a] = text1[a].Split(';');
                var leap = stringToDateTime((string)text1people[a][BirthIndex]);
                
                if (DateTime.IsLeapYear(leap.Year) && 
                    ReturnDate(DateTime.Now - stringToDateTime((string)text1people[a][BirthIndex]))[0]<= 20)
                {
                    Console.WriteLine(text1people[a][NameIndex]);
                }
            }

            Console.ReadKey();
        }
        public static DateTime stringToDateTime(string date)
        {
            
            string[] substrs = date.Split('-');
            int numSubstrs = substrs.Length;
            
            if (numSubstrs == 1)
            {
                
                return new DateTime(int.Parse(substrs[0]),1,1);
            }
            
            return new DateTime(
                int.Parse(substrs[0]),
                int.Parse(substrs[1]),
                int.Parse(substrs[2])
            );
        }

        public static int[] ReturnDate(TimeSpan ts)
        {
            int days = (int)ts.TotalDays;
            int years = 0, months = 0;

            years = days / 365;
            days %= 365;
            months = days / 31;
            days %= 30;

            int[] result = new int[3] { years, months, days }; 

            return result;
        }
    }





}
